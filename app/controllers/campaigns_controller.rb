class CampaignsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_campaign, except: [:index, :create, :new]

  def index
    @campanhas = Campaign.includes('places').all

    respond_to do |format|
      format.html { render layout: false }
      format.json { render json: @campanhas.to_json(include: :places)}
    end
  end

  def show
  end

  def create
  end

  private
    attr_reader :campaign

    def permitted_params
      params.require(:campaign).permit(:name, :description, { :places => [ :name, :address ]})
    end

    def set_campaign
      @campaign = Campaign.find params[:id]
    end

    def authenticate_user!
      redirect_to :root unless current_user
    end
end
