﻿$(function () {
    var
        geocoder,
        map,
        infowindow,

        cid = 0,

        compiledTemplate = $('#infoWindowTemplate').html(),

        template = function (data) {
            return compiledTemplate.replace('{{ADDRESSNAME}}', data.addressName)
                                   .replace('{{ADDRESS}}', data.address)
                                    .replace('{{TITLE}}', data.name)
                                    .replace('{{DESCRIPTION}}', data.description)
                                   .replace('{{ID}}', data.id)
                                    .replace('{{LINK}}', 'http://www.campanhasdobem.com.br/campanhas/' + data.id);
        },

        fetchData = function () {
            $.getJSON('/campaigns/', function (json) {
                var campaigns = json,
                    total = campaigns.length,
                    latLng = [];

                for (var i = 0; i < total; i++) {
                    var totalPlaces = campaigns[i].places.length;

                    for (var j = 0; j < totalPlaces; j++) {
                        var currrentPlace = campaigns[i].places[j];

                        var data = {
                            id: cid++,
                            name: campaigns[i].name,
                            description: campaigns[i].description,
                            addressName: currrentPlace.name,
                            address: currrentPlace.address,
                            latitude: currrentPlace.latitude,
                            longitude: currrentPlace.longitude
                        };

                        latLng.push({
                            latitude: currrentPlace.latitude,
                            longitude: currrentPlace.longitude
                        });

                        app.config.maps.createMarker(map, infowindow, data, template(data));
                    }
                }

                app.config.maps.fitMarkers(map, latLng);
            });
        },

        share = function (e) {
            e.preventDefault();

            var url = $(this).attr('href'),
                title = $(this).siblings('h4').text(),
                description = $(this).siblings('p[data-description]').eq(0).text();

            FB.ui(
              {
                  method: 'feed',
                  name: title,
                  link: url,
                  picture: 'http://www.campanhasdobem.com.br/images/logo.png',
                  description: description
              }
            );
        },

        initialize = function () {
            geocoder = new google.maps.Geocoder();

            app.config.maps.createMap({
                zoom: 6
            }).done(function (result) {
                map = result;
                infowindow = new google.maps.InfoWindow();

                fetchData();


                $(document).on('click', '.share', share);
            });
        };

    google.maps.event.addDomListener(window, 'load', initialize);
});
