NatalFeliz::Application.routes.draw do
  root 'campaigns#index'

  resources :campanhas, controller: 'campaigns'

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'
end
