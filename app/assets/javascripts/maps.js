﻿var app = app || {};
app.config = app.config || {};

app.config.maps = (function () {
    var
        markers = [],

        defaults = {
            center: new google.maps.LatLng(-18.8800397, -47.05878999999999),
            zoom: 8
        },

        options = function (options) {
            var opts = $.extend({}, defaults, options);

            return opts;
        },

        createMarker = function (map, infowindow, data, template) {
            var myLatlng = new google.maps.LatLng(data.latitude, data.longitude);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.name
            });

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(template);
                infowindow.open(map, marker);
            });

            markers.push({
                id: data.id,
                marker: marker
            });
        },

        removeMarker = function (cid) {
            var index = -1;

            for (var i = 0; i < markers.length; i++) {
                if (markers[i].id == cid)
                    index = i;
            }

            markers[index].marker.setMap(null);
            markers.splice(index, 1);
        },

        mapConstructor = function (options) {
            return new google.maps.Map(document.getElementById("map-canvas"), app.config.maps.options(options));
        },

        geoLocation = function (map) {
            navigator.geolocation.getCurrentPosition(
              function (position) {
                  var LatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                  map.setCenter(LatLng);
              }
          );
        },

        createMap = function (options) {
            return $.Deferred(function (def) {
                var map = mapConstructor(options);
                def.resolve(map);

                if (options.location && navigator.geolocation) {
                    geoLocation(map);
                }
            });
        },

        fitMarkers = function (map, data) {
            var latLngList = [];

            for (var i = 0; i < data.length; i++) {
                latLngList.push(new google.maps.LatLng(data[i].latitude, data[i].longitude));
            }

            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, total = latLngList.length; i < total; i++) {
                bounds.extend(latLngList[i]);
            }
            map.fitBounds(bounds);
        };

    return {
        options: options,
        createMarker: createMarker,
        createMap: createMap,
        removeMarker: removeMarker,
        fitMarkers: fitMarkers
    };
})();

